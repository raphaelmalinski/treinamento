#include "treinamento.h"
#include <QPrinter.h>
#include <qpainter.h>
#include <QTextDocument.h>

treinamento::treinamento(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	//m_button = new QPushButton("My Button", this);
	connect(ui.pushButton, SIGNAL (clicked()), this, SLOT (printer()));
}


void treinamento::printer() {
	QPrinter printer(QPrinter::HighResolution); //create your QPrinter (don't need to be high resolution, anyway)
	printer.setPageSize(QPrinter::A4);
	printer.setOrientation(QPrinter::Portrait);
	printer.setPageMargins(15, 15, 15, 15, QPrinter::Millimeter);
	printer.setFullPage(false);
	printer.setOutputFileName("treinamento.pdf");
	printer.setOutputFormat(QPrinter::PdfFormat); //you can use native format of system usin QPrinter::NativeFormat
	QPainter painter; // create a painter which will paint 'on printer'.
	painter.begin(&printer);
	painter.setFont(QFont("Tahoma", 8));
	painter.drawText(200, 200, ui.textEdit->toPlainText());
	painter.end();
}