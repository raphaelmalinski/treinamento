/********************************************************************************
** Form generated from reading UI file 'treinamento.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TREINAMENTO_H
#define UI_TREINAMENTO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_treinamentoClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QTextEdit *textEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *treinamentoClass)
    {
        if (treinamentoClass->objectName().isEmpty())
            treinamentoClass->setObjectName(QString::fromUtf8("treinamentoClass"));
        treinamentoClass->resize(985, 400);
        centralWidget = new QWidget(treinamentoClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(0, 80, 75, 23));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(0, 0, 171, 81));
        treinamentoClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(treinamentoClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 985, 21));
        treinamentoClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(treinamentoClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        treinamentoClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(treinamentoClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        treinamentoClass->setStatusBar(statusBar);

        retranslateUi(treinamentoClass);

        QMetaObject::connectSlotsByName(treinamentoClass);
    } // setupUi

    void retranslateUi(QMainWindow *treinamentoClass)
    {
        treinamentoClass->setWindowTitle(QApplication::translate("treinamentoClass", "treinamento", nullptr));
        pushButton->setText(QApplication::translate("treinamentoClass", "Print", nullptr));
    } // retranslateUi

};

namespace Ui {
    class treinamentoClass: public Ui_treinamentoClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TREINAMENTO_H
